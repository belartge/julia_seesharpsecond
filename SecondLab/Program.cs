﻿using System;

namespace SecondLab
{
    class Program
    {
        static void Main(string[] args)
        {
            var numberOne = new FazzyNumber();
            var numberTwo = new FazzyNumber();
            
            Console.WriteLine("Инициализация первого нечеткого числа");
            numberOne.Input();
            
            Console.WriteLine("Инициализация второго нечеткого числа");
            numberTwo.Input();
            
            Console.WriteLine($"Сложение чисел {numberOne} + {numberTwo} = {numberOne + numberTwo}");
            Console.WriteLine($"Вычитание чисел {numberOne} - {numberTwo} = {numberOne - numberTwo}");
            Console.WriteLine($"Умножение чисел {numberOne} * {numberTwo} = {numberOne * numberTwo}");
            if (IsPositive(numberTwo))
            {
                Console.WriteLine($"Деление чисел {numberOne} / {numberTwo} = {numberOne / numberTwo}");
            }
            else
            {
                Console.WriteLine($"Деление на число {numberTwo} невозможно - оно не является положительным");
            }

            if (IsPositive(numberOne))
            {
                Console.WriteLine($"Инвертированное число {numberOne} = {numberOne.Invert()}");
            }
            else
            {
                Console.WriteLine($"Инвертировать число {numberOne} невозможно - оно не является положительным");
            }

            if (IsPositive(numberTwo))
            {
                Console.WriteLine($"Инвертированное число {numberTwo} = {numberTwo.Invert()}");
            } else
            {
                Console.WriteLine($"Инвертировать число {numberTwo} невозможно - оно не является положительным");
            }

            Console.WriteLine("Нажмите Enter для выхода...");
            Console.ReadLine();
        }

        static bool IsPositive(FazzyNumber number)
        {
            return number.X > 0;
        }
    }
}