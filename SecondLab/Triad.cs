using System;
using System.Text;

namespace SecondLab
{
    public class Triad
    {
        public static int count = 0;

        public double LeftPart { get; set; }

        public double X { get; set; }

        public double RightPart { get; set; }

        public Triad()
        {
            count++;
            LeftPart = 0.0f;
            X = 0.0f;
            RightPart = 0.0f;
        }

        public Triad(double number)
        {
            count++;
            LeftPart = 0.0f;
            X = number;
            RightPart = 0.0f;
        }

        public Triad(double leftPart, double number, double rightPart)
        {
            count++;
            
            // число представляется в виде (leftPart, number, rightPart)
            // приводится к виду (number - LeftPart, number, number + RightNumber)
            
            LeftPart = number - leftPart;
            X = number;
            RightPart = rightPart - number;
        }

        internal void Input()
        {
            Console.Write("Введите левую часть числа = ");
            var leftPart = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите число = ");
            X = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите правое часть числа = ");
            var rightPart = Convert.ToDouble(Console.ReadLine());

            LeftPart = X - leftPart;
            RightPart = rightPart - X;
        }

        protected short Compare(Triad anotherTriad)
        {
            if (anotherTriad == null)
            {
                return 1;
            }
            
            var difference = X - anotherTriad.X;
            if (difference == 0.0)
            {
                return 0;
            }

            if (difference > 0)
            {
                return 1;
            }

            return -1;
        }

        public override string ToString()
        {
            return $"({X - LeftPart}, {X}, {X + RightPart})";
        }

        internal void Display()
        {
            Console.WriteLine("Тройка чисел " + ToString());
        }
    }
}