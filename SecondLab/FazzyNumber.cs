namespace SecondLab
{
    public class FazzyNumber : Triad
    {
        public static FazzyNumber operator +(FazzyNumber firstNumber, FazzyNumber secondNumber)
        {
            // A + B = (A + B – a1 – b1, A + B, A + B + a1 + b1)
            return new FazzyNumber
            {
                LeftPart = firstNumber.X + secondNumber.X - firstNumber.LeftPart - secondNumber.LeftPart,
                X = firstNumber.X + secondNumber.X,
                RightPart = firstNumber.X + secondNumber.X + firstNumber.RightPart + secondNumber.RightPart
            };
        }

        public static FazzyNumber operator -(FazzyNumber firstNumber, FazzyNumber secondNumber)
        {
            // A - B = (A - B – a1 – b1, A - B, A - B + a1 + b1)
            return new FazzyNumber
            {
                LeftPart = firstNumber.X - secondNumber.X - firstNumber.LeftPart - secondNumber.LeftPart,
                X = firstNumber.X - secondNumber.X,
                RightPart = firstNumber.X - secondNumber.X + firstNumber.RightPart + secondNumber.RightPart
            };
        }

        public static FazzyNumber operator *(FazzyNumber firstNumber, FazzyNumber secondNumber)
        {
            // AB = (AB – B a1 – A b1 + a1 b1, AB, AB + B a1 + A b1 + a1 b1)
            return new FazzyNumber
            {
                LeftPart = firstNumber.X * secondNumber.X - secondNumber.X * firstNumber.LeftPart -
                           firstNumber.X * secondNumber.LeftPart + firstNumber.LeftPart * secondNumber.LeftPart,
                X = firstNumber.X * secondNumber.X,
                RightPart = firstNumber.X * secondNumber.X + secondNumber.X * firstNumber.LeftPart +
                            firstNumber.X * secondNumber.LeftPart + firstNumber.LeftPart * secondNumber.LeftPart
            };
        }

        public static FazzyNumber operator /(FazzyNumber firstNumber, FazzyNumber secondNumber)
        {
            // A / B = ((A – a1)/(B + b1), A/B, (A + a1)/(B – b1))
            return new FazzyNumber
            {
                LeftPart = (firstNumber.X - firstNumber.LeftPart) / (secondNumber.X + secondNumber.RightPart),
                X = firstNumber.X / secondNumber.X,
                RightPart = (firstNumber.X + firstNumber.RightPart) / (secondNumber.X - secondNumber.LeftPart)
            };
        }

        public FazzyNumber Invert()
        {
            // A = (1 / ( A + a1), 1/ A, 1 ( A – a1))
            return new FazzyNumber
            {
                LeftPart = 1 / (X + RightPart),
                X = 1 / X,
                RightPart = 1 / (X - LeftPart)
            };
        }
    }
}